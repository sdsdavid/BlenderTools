# Before creating an issue DO THIS FIRST!
Search the existing [issues](https://github.com/EpicGames/BlenderTools/issues?q=)



## Bug
### IMPORTANT: Do this before you create a bug ticket!
  1. Remove the addon.
  1. Shutdown Blender.
  1. Download and install the latest addon version.
  1. Start Blender.
  1. Install the latest addon.
  1. If the bug still exists, report it by copying the info below into a new issue.

Create a **Title** with the addon name first i.e. Send to Unreal - ```<your title>```

Use the **'Bug'** Label

### In the description paste in and edit the following:
* **Addon:** i.e Send to Unreal
* **Steps to Reproduce the Problem:** i.e. I clicked Pipeline > Export > Send to Unreal
* **Error Log:** Paste the full error log
* **OS:** i.e.Windows, Mac, Linux
* **Addon Version:** i.e. Send to Unreal 1.4.0
* **Blender Version:** i.e. Blender 2.83
* **Unreal Version:** i.e. Unreal 4.25


## Feature
### IMPORTANT: Do this before you create a feature ticket!
  1. Search the Documentation.
  1. Watch all the videos in the documentation.
  1. If the action you are trying to do is currently not possible, or you think a pretty good case can be made for having a specific set of actions automated, create an issue with a **'discussion'** label so other users can chime in and discuss the new workflow the new feature. 
  1. Once the workflow is agreed on create a new feature issue close by copying the info below. Leave links to the new feature issues, and then close the discussion issue.

Create a **Title** with the addon name first i.e. UE to Rigify - ```<your title>```

Use the **'Feature'** Label

### In the description paste in and edit the following:
* **Addon:** i.e UE to Rigify
* **Workflow:** i.e. Select a bone, then hit a hot key and get a pie menu with 2 options.
* **Description:** i.e. For this two work you would need to create 2 new operator that are specific to only the 3D view... etc.


## Questions
### IMPORTANT: Do this before you create a question ticket!
  1. Search the Documentation.
  1. Search the FAQ to see if your question was already asked.
  1. If question you have is not covered in the documentation or the FAQ, feel free to create a issue.

Create a **Title** with the addon name first i.e. UE to Rigify - ```<your title>```

Use the **'Question'** Label

### In the description paste in and edit the following:
* **Addon:** i.e UE to Rigify
* **Question:** i.e. How do you export a template?